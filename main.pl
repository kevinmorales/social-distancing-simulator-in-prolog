%*****************************************************************************
% Project Description: You’re trying really hard to adhere to social distancing 
% rules, but sometimes your visits to the local park aren’t so easy. You’re trying
% to get from a point on the south side of the park to a nice patch of grass on 
% the north side. There are several people around and sometimes they aren’t 
% trying as hard as you to do the right thing. But you’ve had a great idea!
% You launch a drone and create a map of the current location of all of the
% people in the park. Now you just have to write a little program to find 
% a path such that you stay 6 feet away from every other park-goer!
% 
% Constraints: User can only move forward, right, or left, but never backwards. 
% 			   User must always be six feet away from other park goers. 
% Parameters: Initial Coordinates
%  			  Ending y Coordinate
%  			  Dimensions of park
% 			  Locations of other people at the park (Could be 0 to many)
% 			  The safe path through the park (Initially an unknown variable)
% Returns: A List with Coordinates of the safe path through the park.
% 
% Test Cases: 1. Schlegel's given case
% 				Locations = [[20,4],[13,7],[4,19]]
% 				solve([13,0],24,[25,25],[[20,4],[13,7],[4,19]],P).
% 				
% 			  2. No one in my path case
% 			   Locations = []
% 			   solve([13,0],24,[25,25],[],P).
% 			   
% 			  3. Impossible case
% 			   Locations = [[0,7],[13,7],[0, 20]]
% 			   solve([13,0],24,[25,25],[[7,0],[13,7],[20,0]],P).
%
%*****************************************************************************
solve([InitialX,InitialY], 
      EndingYCoordinate, 
      [UpperXBound, UpperYBound], 
      Locations, 
      SafePath) :-
	findSafePath(
        	% Initial Coordinates
        	[InitialX, InitialY],
            % Ending Y Coordinate (goal)
            EndingYCoordinate, 
            % Dimensions of the park
            [UpperXBound, UpperYBound],
            % List with locations of other people at park
            Locations,
            % Set the current path to the initial coordinates
            [[InitialX,InitialY]], 
            % Safe path is what we are solving for
            SafePath).

%*****************************************************************************
% Description: This function calls several other functions to determine
% 	the safest path across the park. This function is recursive, which 
% 	requires a base case. The algorithm for finding a safe path is as 
% 	follows: Move to one foot in a direction, check if that new location 
% 	is safe, check if that location is within the bounds of the park, 
% 	check if that loaction has been visited already, if all are true
% 	then append these new coordinates to the ned of the safe path list, 
%	Finally, do a recursive call to test another set of coordinates to 
%	move to(This will happen regardless of whether or not we were able 
%	to find a safe path this time. 
% Parameters: Current user coordinates
%  			  Ending y Coordinate
%  			  Dimensions of park
% 			  Locations of other people at the park (Could be 0 to many)
% 			  The safe path through the park
% 			  The final safe path through the park (a passenger variable until we reach the base case)
% Returns: A List with Coordinates of the safe path through the park.
%
%*****************************************************************************

% Base case, We have reach the top of the park, 
findSafePath(
             [_, CurrentY], 
             EndingYCoordinate,
             _,
         	 _,
            Path, 
            SafePath) :- 
    % Current y coordinate is equal to the ending y coordinate
    CurrentY is EndingYCoordinate,
    % The current path is know to be a safe path through the park, it is the safe path
    SafePath= Path.	
    
findSafePath(
             % Current user coordinates
             [CurrentX, CurrentY],
             % Ending y coordinate
             EndingYCoordinate, 
             % Dimensions of park
             [UpperXBound, UpperYBound], 
             % Locations of other park goers
             Locations, 
             % Current safe path through the park
             Path, 
             % Final Safe path through the park
             SafePath) :-
    % Move one foot in a direction
	move([CurrentX,CurrentY],[NextX,NextY],[UpperXBound, UpperYBound]),
    % Check if out of range of other people at the park
    distanceCheck(Locations, [NextX,NextY]),
    % Check if this new location is already in our path
    \+member([NextX,NextY], Path),
    % Add new location to the end of the list with safe locations
    append(Path,[[NextX,NextY]], NewPath), 
    % Recursive call to test another new location
	findSafePath([NextX,NextY],
                 EndingYCoordinate, 
                 [UpperXBound, UpperYBound], 
                 Locations, 
                 NewPath, 
                 SafePath).

%***************************************************************
% Description: This function checks to see if the new coordinates
% 	of the user are within the range of any of the other park goers.
% 	There is a 0 to many relation between the park and the number of
% 	other park goers. A recursive call, that has a base case of no park
% 	goers left to check, is used to check the locations of all other 
% 	park goers. This function determines if a given set of coordinates
% 	are within a given range of another given set of coordinates. Using
%	Pythagorean theorem, we can use the two sets of coordinates to form
%	a triangle, and solve for the hypotenuse, which is our range.
%	https://www.cpp.edu/~jrfisher/www/prolog_tutorial/2_7.html
% Parameters: List with locations of other park goers
% 	 		  User's current location
%
%***************************************************************

% Base case for recursion, occurs if there are no locations or no locations left to check
distanceCheck([], _).

% Check if coordinates of user are within 6 feet of the current location of another park goer
distanceCheck([[OtherPersonX,OtherPersonY]|Locations], [CurrentX,CurrentY]) :-
    6 < (((OtherPersonX - CurrentX)^2) + (OtherPersonY - CurrentY)^2)^(1/2),	
	distanceCheck(Locations,[CurrentX,CurrentY]).


%***************************************************************************
% Description: This function moves the current coordinates of
% 	the user by one in a given direction. The user can only 
% 	move forward, left, or right. In an ideal situation, there
% 	are no park goers in the way of the user, so a straight 
% 	forward path will be taken. For this reason the order of 
% 	the functions is made so that an attempt to move forward
% 	is tried first. This function checks if a given coordinate
% 	pair is within the bounds defined by the user. 
% Parameters: Array with current coordinates, Array with next coordinates
% Returns: Array with next coordinates after movement
% 
%***************************************************************************

% Move forward
move([CurrentX,CurrentY],[NextX,NextY],[_,UpperYBound]) :-
    % Set next x coordinate to current x coordinate
    NextX is CurrentX,
    % Set next y coordinate to current y coordinate + 1
    NextY is CurrentY + 1,
    NextY >= 0,
    NextY =< UpperYBound.		

% Move left
move([CurrentX,CurrentY],[NextX,NextY],[UpperXBound,_]) :-
    % Set next x coordinate to current x coordinate + 1
    NextX is CurrentX - 1,
    NextX >= 0,
    NextX =< UpperXBound,
    % Set next y coordinate to current y coordinate
    NextY is CurrentY.	

% Move right
move([CurrentX,CurrentY],[NextX,NextY],[UpperXBound,_]) :-
    % Set next x coordinate to current x coordinate + 1
    NextX is CurrentX + 1,
    NextX >= 0,
    NextX =< UpperXBound,
    % Set next y coordinate to current y coordinate
    NextY is CurrentY.
